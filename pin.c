/**
 * Copyright (c) 2016 Jacob Greenwood
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy 
 *  of this software and associated documentation files (the "Software"), to deal 
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do
 *  so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 *  PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 *  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 *  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * This code is a part of the PicLibraries project, see bitbucket.org/jako77/piclibraries 
 *  for more information
 **/
#include "pin.h"

int8_t pin_mode(uint8_t pin, uint8_t mode)
{
    if (mode != INPUT && mode != OUTPUT)
    {
        return -2;
    }
    
    if (pin < 8)
    {
        TRISA = (TRISA & ~(1<<pin))
                | (mode<<pin);
    }
    else if (pin < 16)
    {
        TRISB = (TRISB & ~(1<<(pin-8)))
                | (mode<<(pin-8));
    }
    else if (pin < 24)
    {
        TRISC = (TRISC & ~(1<<(pin-16)))
                | (mode<<(pin-16));
    }
    else
    {
        return -1;
    }
    
    return 0;
}

int8_t pin_write(uint8_t pin, uint8_t high)
{
    if (high != HIGH && high != LOW)
    {
        return -2;
    }
    
    if (pin < 8)
    {
        LATA = (LATA & ~(1<<pin))
                | (high<<pin);
    }
    else if (pin < 16)
    {
        LATB = (LATB & ~(1<<(pin-8)))
                | (high<<(pin-8));
    }
    else if (pin < 24)
    {
        LATC = (LATC & ~(1<<(pin-16)))
                | (high<<(pin-16));
    }
    else
    {
        return -1;
    }
    
    return 0;
}

int8_t pin_read(uint8_t pin)
{
    if (pin < 8)
    {
        if (PORTA & (1<<pin))
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
    else if (pin < 16)
    {
        if (PORTB & (1<<(pin-8)))
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
    else if (pin < 24)
    {
        if (PORTA & (1<<(pin-16)))
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
    else
    {
        return -1;
    }
}

