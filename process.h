/**
 * Copyright (c) 2016 Jacob Greenwood
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy 
 *  of this software and associated documentation files (the "Software"), to deal 
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do
 *  so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 *  PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 *  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 *  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * This code is a part of the PicLibraries project, see bitbucket.org/jako77/piclibraries 
 *  for more information
 **/

#ifndef PROCESS_H
#define	PROCESS_H

#include <stdbool.h>
#include "../config.h"

#ifndef PROCESS_ARRAY_SIZE
    #define PROCESS_ARRAY_SIZE 10
#endif

#ifdef	__cplusplus
extern "C" {
#endif

typedef struct
{
    bool active;
    int timeDelayMS;
    int timeLeftMS;
    char* description;
    void ( *func )( void );
} Process;

/* Public Functions */
void process_init();
void process_runProcesses();
int process_addProcess(int timeMS, char* description, void (*func)(void));

/* Private Functions */
void process_defaultCallback();
int process_getFirstOpen();

#ifdef	__cplusplus
}
#endif

#endif	/* PROCESS_H */

