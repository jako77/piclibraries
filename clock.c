/**
 * Copyright (c) 2016 Jacob Greenwood
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy 
 *  of this software and associated documentation files (the "Software"), to deal 
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do
 *  so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 *  PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 *  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 *  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * This code is a part of the PicLibraries project, see bitbucket.org/jako77/piclibraries 
 *  for more information
 **/

#include "clock.h"

uint32_t clock_timeUS = 0;
/// A timer needs to be configured to call this callback
/// A good way to do this would be to have the callback function setup as a macro define in the projConfig.h file
void clock_callback(uint16_t numUS)
{
    clock_timeUS += numUS;
}

void clock_delayMS(uint32_t ms)
{
    /// @todo goto sleep here
    uint32_t startTime = clock_getTimeMS();
    while(clock_getTimeMS() < startTime + ms);
}

void clock_delayUS(uint32_t us)
{
    uint32_t startTime = clock_getTimeUS();
    while(clock_getTimeUS() < startTime + us);
}

uint32_t clock_getTimeMS()
{
    static uint32_t returnTime = 0;
    
    // disable interrupts
    INTERRUPT_GlobalInterruptDisable();
    
    returnTime = clock_timeUS/1000;
    
    // re-enable interrupts
    INTERRUPT_GlobalInterruptEnable();
    
    return returnTime;
    
    // This compile error may be due to a lack of registers or something in the compiler
}

uint32_t clock_getTimeUS()
{
    static uint32_t returnTime = 0;
    
    // disable interrupts
    INTERRUPT_GlobalInterruptDisable();
    
    returnTime = clock_timeUS;
    
    // re-enable interrupts
    INTERRUPT_GlobalInterruptEnable();
    
    return returnTime;
}
