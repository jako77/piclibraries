/**
 * Copyright (c) 2016 Jacob Greenwood
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy 
 *  of this software and associated documentation files (the "Software"), to deal 
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do
 *  so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 *  PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 *  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 *  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * This code is a part of the PicLibraries project, see bitbucket.org/jako77/piclibraries 
 *  for more information
 * 
 * This example config file should be renamed to "config.h" and placed one level above the PicLibraries folder (typically with main.c)
 *
 **/

#ifndef CONFIGURATION_H
#define	CONFIGURATION_H

#include <xc.h>
#include "mcc_generated_files/mcc.h"

#include "PicLibraries/clock.h"
#include "PicLibraries/process.h"
#include "PicLibraries/pwm.h"
#include "PicLibraries/pin.h"

#ifdef	__cplusplus
extern "C" {
#endif

// Defines the function to be used as a callback, the input could be 16bit
#define CLOCK_CALLBACK(numUS) clock_callback(numUS)
#define PWM_CALLBACK(numUS) pwm_callback(numUS)
    
#ifdef	__cplusplus
}
#endif

#endif	/* CONFIGURATION_H */

