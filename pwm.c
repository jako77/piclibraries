/**
 * Copyright (c) 2016 Jacob Greenwood
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy 
 *  of this software and associated documentation files (the "Software"), to deal 
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do
 *  so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 *  PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 *  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 *  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * This code is a part of the PicLibraries project, see bitbucket.org/jako77/piclibraries 
 *  for more information
 **/

#include "pwm.h"

PWM_CHANNEL pwm_channels[PWM_CHANNELS_MAX];

int8_t pwm_addPin(uint8_t pin, uint32_t totalPeriodUS)
{
    int8_t channelIndex = pwm_findFirstUnusedChannel();
    if (channelIndex >= 0)
    {
        pwm_channels[channelIndex].active = true;
        pwm_channels[channelIndex].highTimeUS = 0;
        pwm_channels[channelIndex].periodUS = totalPeriodUS;
        pwm_channels[channelIndex].pin = pin;
        
        pin_mode(pin, OUTPUT);
        pin_write(pin, LOW);
    }
    
    return 0;
}

void pwm_removePin(uint8_t pin)
{
    int8_t channel = pwm_findFirstChannelWithPin(pin);
    pwm_channels[channel].active = false;
}

void pwm_set(uint8_t pin, uint16_t dutyCycle)
{
    uint8_t channel = pwm_findFirstChannelWithPin(pin);
    
    if (dutyCycle > PWM_MAX_DUTY_CYCLE)
    {
        dutyCycle = PWM_MAX_DUTY_CYCLE;
    }
    
    pwm_channels[channel].highTimeUS = pwm_channels[channel].periodUS * dutyCycle / PWM_MAX_DUTY_CYCLE;
}

uint16_t pwm_getMaxDutyCycle()
{
    return PWM_MAX_DUTY_CYCLE;
}

int8_t pwm_findFirstUnusedChannel()
{
    uint8_t i;
    for (i = 0; i < PWM_CHANNELS_MAX; i++ )
    {
        if (!pwm_channels[i].active)
        {
            return i;
        }
    }
    
    return -1;
}

int8_t pwm_findFirstChannelWithPin(uint8_t pin)
{
    uint8_t i;
    for (i = 0; i < PWM_CHANNELS_MAX; i++)
    {
        if (pwm_channels[i].active && pwm_channels[i].pin == pin)
        {
            return i;
        }
    }
    
    return -1;
}

uint32_t pwm_time_us = 0;
void pwm_callback(uint16_t numUS)
{
    pwm_time_us += numUS;

    uint8_t i;
    for (i = 0; i < PWM_CHANNELS_MAX; i++)
    {
        if (pwm_channels[i].active && pwm_channels[i].highTimeUS != 0)
        {
            uint32_t timeAfterStart = pwm_time_us % pwm_channels[i].periodUS;
            if (timeAfterStart < pwm_channels[i].highTimeUS)
            {
                pin_write(pwm_channels[i].pin, HIGH);
            }
            else
            {
                pin_write(pwm_channels[i].pin, LOW);
            }
        }
    }
}