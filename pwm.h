/**
 * Copyright (c) 2016 Jacob Greenwood
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy 
 *  of this software and associated documentation files (the "Software"), to deal 
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do
 *  so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 *  PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 *  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 *  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * This code is a part of the PicLibraries project, see bitbucket.org/jako77/piclibraries 
 *  for more information
 **/

#ifndef PWM_H
#define	PWM_H

#include "../config.h"

typedef struct
{
    bool active;
    uint8_t pin;
    uint32_t highTimeUS;
    uint32_t periodUS;
} PWM_CHANNEL;

#define PWM_CHANNELS_MAX 4
#define PWM_PERIOD_US pwm_period_us;
#define PWM_MAX_DUTY_CYCLE 1024

#ifdef	__cplusplus
extern "C" {
#endif

/* Public Functions */
int8_t pwm_addPin(uint8_t pin, uint32_t totalPeriodUS);
void pwm_removePin(uint8_t pin);
void pwm_set(uint8_t pin, uint16_t dutyCycle);
uint16_t pwm_getMaxDutyCycle();

/* Private Functions */
void pwm_callback(uint16_t numUS);
int8_t pwm_findFirstUnusedChannel();
int8_t pwm_findFirstChannelWithPin(uint8_t pin);

#ifdef	__cplusplus
}
#endif

#endif	/* PWM_H */

