/**
 * Copyright (c) 2016 Jacob Greenwood
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy 
 *  of this software and associated documentation files (the "Software"), to deal 
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do
 *  so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 *  PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 *  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 *  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * This code is a part of the PicLibraries project, see bitbucket.org/jako77/piclibraries 
 *  for more information
 **/

#include "process.h"

Process process_processArray[PROCESS_ARRAY_SIZE];

void process_defaultCallback()
{
    // this shouldn't ever be called
    __debug_break();
}

void process_init()
{
    for ( int i = 0; i < PROCESS_ARRAY_SIZE; i++ )
    {
        process_processArray[i].active = false;
        process_processArray[i].timeLeftMS = 0;
        process_processArray[i].func = process_defaultCallback;
    }
}

void process_runProcesses()
{
    static long currentTimeMS;
    static long lastTimeMS = 0;
    static int timePassedMS;

    currentTimeMS = clock_getTimeMS();
    timePassedMS = currentTimeMS - lastTimeMS;

    for ( int i = 0; i < PROCESS_ARRAY_SIZE; i++ )
    {
        if ( process_processArray[i].active == true )
        {
            process_processArray[i].timeLeftMS -= timePassedMS;
            if (process_processArray[i].timeLeftMS <= 0)
            {
                process_processArray[i].func();
                process_processArray[i].timeLeftMS = process_processArray[i].timeDelayMS;
            }
        }
    }

    lastTimeMS = currentTimeMS;
}

int process_getFirstOpen()
{
    for ( int i = 0; i < PROCESS_ARRAY_SIZE; i++ )
    {
        if ( process_processArray[i].active == false )
        {
            return i;
        }
    }
    
    ///  @todo Use generic error message library for return here
    return -1;
}

int process_addProcess(int timeMS, char* description, void (*func)())
{
    int openProcessNum = process_getFirstOpen();
    if ( openProcessNum < 0 )
        return openProcessNum;
    
    process_processArray[openProcessNum].active = true;
    process_processArray[openProcessNum].description = description;
    process_processArray[openProcessNum].timeDelayMS = timeMS;
    process_processArray[openProcessNum].func = func;
    
    return 0; /// @todo
}